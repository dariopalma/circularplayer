public class AutoPlayer
{
  private Bob _joueur;
  private PVector _centre;
  private float _rayonMax;
  private float _vitesse;
  private CercleSetter _cs;
  
  public AutoPlayer(Bob j, float r, PVector c, float v, CercleSetter cs)
  {
    this._joueur = j;
    this._centre = c;
    this._rayonMax = r;
    this._vitesse = v;
    this._cs = cs;
  }
  
  public void jouer()
  {
    float distance = dist(_joueur.location.x,_joueur.location.y,_centre.x,_centre.y);
    if(!_joueur.dragging && distance < _rayonMax/2)
    {
      float vitesse = _cs.valeurNormalise();
      if(vitesse > 0)
      {
        float s = sin(_vitesse*vitesse);
        float c = cos(_vitesse*vitesse);
        
        _joueur.location.x -= _centre.x;
        _joueur.location.y -= _centre.y;
        
        float nx = _joueur.location.x*c - _joueur.location.y*s;
        float ny = _joueur.location.x*s + _joueur.location.y*c;
        
        _joueur.location.x = nx + _centre.x;
        _joueur.location.y = ny + _centre.y;
      }
    }
  }
  
}