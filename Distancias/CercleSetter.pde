/**
 * Class Cercle Setter
 * Elle nous permet de régler un paramètre avec la souris
 * Par contre elle peut qu'être verticale ou sinon les fonctions ne marcheront pas
 */
class CercleSetter
{
  private PVector _debut;
  private PVector _fin;
  private float _rayon;
  private PVector _position;
  private PVector _traineeOffset = new PVector();
  private boolean _trainee = false;
  private String _nom = "";

  /**
   * Constructeur de la classe
   * Cree une ligne avec une balle aux coordonnés du début
   * @param debutX coordonné initiale en X
   * @param debutY coordonné initiale en Y
   * @param finX coordonné finale en X
   * @param finY coordonné finale en Y
   * @param rayon de la balle
   */
  public CercleSetter(float debutX, float debutY, float finX, float finY, float rayon)
  {
    this._debut = new PVector(debutX,debutY);
    this._fin = new PVector(finX,finY);
    this._rayon = rayon;
    this._position = new PVector(debutX,debutY);
  }
  
  public void nommer(String nom)
  {
    this._nom = nom;
  }
  
  /**
   * Fonction d'affichage
   */
  public void display()
  {    
    textSize(32);
    fill(0, 102, 153);
    text(_nom,_fin.x-textWidth(_nom)/2,_fin.y-16);
    
    
    line(_debut.x,_debut.y,_fin.x,_fin.y);
    fill(200);
    ellipse(_position.x,_position.y,_rayon*2,_rayon*2);
  }
  
  /**
   * Au moment d'être clické par la souris nous changons son état à traînée
   */
  public void clicked(int mx, int my)
  {
    if(dist(mx,my,_position.x,_position.y) < _rayon)
    {
      _trainee = true;
      //_traineeOffset.x = _position.x-mx;
      _traineeOffset.y = _position.y-my;
    }
  }

  /**
   * Si la balle n'est plus traînée nous changons son état
   */
  public void stopDragging()
  {
    _trainee = false;
  }
  
  /**
   * Quand la balle est appuyé en dessus il faut changer sa position
   * mais il faut toujour rester sur la ligne
   * @param mx souris en x
   * @param my souris en y
   */
  public void drag(int mx, int my)
  {
    if (_trainee)
    {
      //float resultatX = mx + _traineeOffset.x;
      float resultatY = my + _traineeOffset.y;
      if( _debut.y > resultatY && resultatY > _fin.y)
      {
        _position.y = resultatY;
      }
    }
  }
  
  /**
   * Cette fonction nous donne le valeur de notre paramètre répresenté 
   * @return 
   */
  public float valeurNormalise()
  {
    float echelle = _fin.y - _debut.y;
    return (_position.y -  _debut.y)/(echelle);
  }
  
}