/**
 * Class Cercle Centrale
 * Elles nous permet de jouer notre morçeau de musique et afficher les notes qui sont jouées à l'instant 
 */
class CercleCentrale{
  private SongInterface _notes;
  private float _divisions = 0;
  private float _rayon = 1;
  private Bob[] _bobs;
  private PVector _centre;
  private PVector _rayonPos;
  private ArrayList<Integer> _noteJoue;
  /**
   * Constructeur de la classe
   * Elle nous permet d'ajouter les parametres d'affichage pour notre piano circulaire
   * @param da correspond aux divisions initiales
   * @param rayon represent le rayon des arcs
   */
  public CercleCentrale(SongInterface song, float rayon)
  {
    this._notes = song;
    this._rayon = rayon;
    this._divisions = song.getTotalTime();
    this._centre = new PVector(width/2,height/2);
    this._rayonPos = new PVector(width/2+2*rayon,height/2);
    this._noteJoue = new ArrayList<Integer>();
  }
  
  /**
   * On ajoute les references de Bobs et on modifie la taille des notes à jouer
   * @param bobs c'est le vecteur des references
   */
  public void ajouterBobs( Bob[] bobs)
  {
    _bobs = bobs;
  }
  
  /**
   * Function afficher
   * Elle comprend aussi l'affichage des couleurs différentes  
   */
  public void display()
  { 
    _noteJoue.clear();
    float deltaAngle = 2*PI/_divisions;
    
    int couleur, temp; // couleur et temps du note
    int noteIndex = 0;
    
    for(int note = 1; note <= _divisions; note++)
    {
      temp = _notes.getNoteTime(noteIndex);
      couleur = 250;
      for(int cetBob = 0; cetBob < bobs.length; cetBob++)
      {
        if(dist(bobs[cetBob].location.x,bobs[cetBob].location.y,_centre.x,_centre.y) < _rayon)
        {
          if(bobMarcheDessus(note,temp,deltaAngle,cetBob))
          {
            _noteJoue.add(noteIndex);
            couleur = 40;
            break;
          }
        }
      }
      fill(couleur);
      
      arc(_centre.x,_centre.y,2*_rayon,2*_rayon,deltaAngle*(note-1),deltaAngle*(note+temp-1),PIE);
      note = note + temp-1;
      noteIndex++;
    }
  }
  
  /**
   * Function pour donner les notes jouées cette frame
   * S'il n'y a pas de note jouée la fonction ajoutera le silence
   * @return la liste de notes jouées
   */
  public String[] notesJoues()
  {
    String[] notes = new String[bobs.length];
    for( int i = 0; i <_noteJoue.size(); i++)
    {
      notes[i] = _notes.getNote(_noteJoue.get(i));
    }
    
    if(_noteJoue.size() < bobs.length)
    {
      for(int i = _noteJoue.size(); i < bobs.length; i++)
      {
        notes[i] = "Silence";
      }
    }
    
    return notes;
  }
  
  
  public void pause()
  {
    delay(5000);
  }
  
  /**
   * Function pour vérifier qu'il y a un Bob sur la note
   * @param i compteur d'area actuelle
   * @param deltaAngle pour connaitre l'area actuelle
   * @param b_i index du Bob 
   * @return si la zone actuelle a un Bob au dessus
   */
  private boolean bobMarcheDessus(int i, int t, float deltaAngle, int b_i)
  {   
    Bob b = _bobs[b_i];
    PVector v1,v2;
    v1 = PVector.sub(_centre,_rayonPos);
    v2 = PVector.sub(_centre,b.location);
    float angle = PVector.angleBetween(v1,v2);
    float lim1 = ((i-1)*deltaAngle)%(2*PI);
    float lim2 = ((i+t-1)*deltaAngle)%(2*PI);
    if(b.location.y < _centre.y)
    {
      angle = 2*PI - angle;
      if(lim2 == 0)
      {
        lim2 = 2*PI;
      }
    }

    if(lim1 < angle && angle < lim2)  
    {
      return true;
    }
    return false;
  }
  
  public PVector getCentre()
  {
    return this._centre;
  }
  
  public float getRayon()
  {
    return this._rayon*2;
  }
  
}