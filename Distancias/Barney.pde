public class Barney extends SongAbstract
{  
  public Barney(){
    
    String[] notes = {"G4","E4","G4","E4","G4#",
                      "A4","G4","F4","E4","D4","E4","F4",
                      "E4","F4","G4","C4","D4","F4","G4",
                      "G4","D4","F4","E4","D4","C4"};
                        
    int temps[] = {1,1,2,1,1,
                   1,1,1,1,1,1,1,
                   1,1,1,5,1,1,1,
                   1,2,1,1,1};
                      
    super._notes = new String[notes.length];
    super._temps = new int[temps.length];
    System.arraycopy( notes, 0, super._notes, 0, notes.length );
    System.arraycopy( temps, 0, super._temps, 0, temps.length );
     
    _taille = 0;
    for(int i = 0; i < _temps.length; ++i)
    {
      _taille += _temps[i];
    }
    _notesTotales = _notes.length;
  } 
}