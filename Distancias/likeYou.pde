public class LikeYou extends SongAbstract
{
  public LikeYou(){
  
    String[] notes = {"A3","C3#","E3","C3#",
                      "A3","C3#","E3","C3#",
                      "A3","C3#","E3","C3#",
                      "A3","C3#","E3","C3#",
                       
                      "G3#","C3#","E3","C3#",
                      "G3#","C3#","E3","C3#",
                      "G3#","C3#","E3","C3#",
                      "G3#","C3#","E3","C3#",
                      
                      "F3#","C3#","F3#","C3#",
                      "F3#","C3#","F3#","C3#",
                      "F3#","C3#","F3#","C3#",
                      "F3#","C3#","F3#","C3#",
                      
                      "A3","D3","F3#","D3",
                      "A3","D3","F3#","D3",
                      "A3","D3","F3#","D3",
                      "A3","D3","F3#","D3",
                      };
                        
    int temps[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
                   };
                      
    super._notes = new String[notes.length];
    super._temps = new int[temps.length];
    System.arraycopy( notes, 0, super._notes, 0, notes.length );
    System.arraycopy( temps, 0, super._temps, 0, temps.length );
    
    _taille = 0;
    for(int i = 0; i < _temps.length; ++i)
    {
      _taille += _temps[i];
    }
    _notesTotales = _notes.length;
  }
}