public class Anniversaire extends SongAbstract
{
  
  public Anniversaire(){
    
    String[] notes = {"A3","B3","A3","D3","C3#",
                      "A3","B3","A3","D3",
                      "A3","F3#","D3","C3#","B3",
                      "G3","F3#","D3","E3","D3"};
                    
    int temps[] = {2,1,1,1,3,
                   2,1,1,5,
                   3,1,1,1,3,
                   2,1,1,1,3};
                      
    super._notes = new String[notes.length];
    super._temps = new int[temps.length];
    System.arraycopy( notes, 0, super._notes, 0, notes.length );
    System.arraycopy( temps, 0, super._temps, 0, temps.length );
    
    _taille = 0;
    for(int i = 0; i < _temps.length; ++i)
    {
      _taille += _temps[i];
    }
    _notesTotales = _notes.length;
  }
  
}