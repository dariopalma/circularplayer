public interface SongInterface
{
  public int getTotalTime();
  
  public int getNotes();
  
  public String getNote(int index);
  
  public int getNoteTime(int index);
  
  public int[] getTimeArray();
}

public abstract class SongAbstract implements SongInterface
{
  protected String[] _notes;
  
  protected int[] _temps;
  
  protected int _taille;
  
  protected int _notesTotales;
  
  public int getTotalTime()
  {
    return _taille;
  }
  
  public int getNotes()
  {
    return _notesTotales;
  }
  
  public int getNoteTime(int index)
  {
    return _temps[index];
  }

  
  public String getNote(int index)
  {
    return _notes[index];
  }
  
  public int[] getTimeArray()
  {
    return _temps;
  }
}