/******************************************************************************
                             Réalité Virtuelle
                        Mini Projet Immersion Sonore
                            @autor dario & jose
                              @date 13/12/2016
******************************************************************************/
import oscP5.*;
import netP5.*;


// Elements pour maintenir la communication avec pure data
OscP5 oscP5;
NetAddress myRemoteLocation;

// Taille
int tailleBobs = 3;
// Objets qui jouent de la musique
Bob[] bobs = new Bob[tailleBobs];
Spring[] springs = new Spring[tailleBobs-1];
float[] distances = new float[tailleBobs-1];

// Elements musicales
Frequences freq = new Frequences();
Anniversaire anivSong = new Anniversaire();
Barney barney = new Barney();
LikeYou like = new LikeYou();

// Controlleurs 
CercleCentrale cc;
CercleSetter volume, vitesse;

AutoPlayer auto;


/**
 * La methode pour initializer notre application
 */
void setup() {
  
  // Taille de l'écran
  size(800, 360);
  
  freq.ajouterOctave(4);
 
  // Les controlleurs
  cc = new CercleCentrale(like,150);
  volume = new CercleSetter(600,300,600,100,20);
  volume.nommer("Vol");
  vitesse = new CercleSetter(50,300,50,100,20);
  vitesse.nommer("Vit");
 
 // Les elements interactifs
  for (int i = 0; i < bobs.length; i++) {
    bobs[i] = new Bob(640/2, (tailleBobs*40)-i*40);  
  }
  for (int i = 0; i < springs.length; i++) {
    springs[i] = new Spring(bobs[i], bobs[i+1],40);
  }

  auto = new AutoPlayer(bobs[0],cc.getRayon(),cc.getCentre(),PI/16,vitesse);
  
  // Les elements qui nous permet de envoyer l'info vers pure data
  oscP5 = new OscP5(this,12000);
  myRemoteLocation = new NetAddress("127.0.0.1",1234);
   
  // Nous ajoutons les réferences au controlleur
  cc.ajouterBobs(bobs);
  
}


/**
 * Fonction pour afficher l'image 
 */
void draw() {
  
  background(255); 
  
  cc.display();
  
  volume.display();
  
  vitesse.display();

  for (Spring s : springs) {
    s.update();
    s.display();
  }

  for (Bob b : bobs) {
    b.update();
    b.display();
    b.drag(mouseX, mouseY);
  }
  
  volume.drag(mouseX,mouseY);
  vitesse.drag(mouseX,mouseY);
  
  auto.jouer();
  
  envoyerMessages();
}

/**
 * Nous preparons et envoyons le messsage
 */
void envoyerMessages()
{
  // Créer le message
  OscMessage myMessage = new OscMessage("/test");
  
  // Generer un son stéréo en fonction des positions en X des Bobs
  float gauche, droit = 0;
  for (int i=0; i < bobs.length; i++){
    droit += bobs[i].location.x;
  }
  droit = droit / (width*3);
  gauche = 1 - droit;
 
 
  myMessage.add(droit);
  myMessage.add(gauche);
  
  // Envoyer Notes Jouées
  String[] notes = cc.notesJoues();
  for (int i=0; i < notes.length; i++){
      myMessage.add(freq.getFreq(notes[i]));
  }
  
  // Modifier volume
  myMessage.add(volume.valeurNormalise());
 
 
  // Envoyer les messages
  oscP5.send(myMessage, myRemoteLocation);
}

/**
 * Actions par réponse au evenement de s'appuyer sur la souris
 */
void mousePressed() {
  for (Bob b : bobs) {
    b.clicked(mouseX, mouseY);
  }
  volume.clicked(mouseX,mouseY);
  vitesse.clicked(mouseX,mouseY);
}

/**
 * Actions par réponse au evenement de rélacher la souris
 */
void mouseReleased() {
  for (Bob b : bobs) {
    b.stopDragging();
  }
  volume.stopDragging();
  vitesse.stopDragging();
}