import java.util.Map;

/**
 * Classe Frequences
 * A l'aide d'un HashMap nous pouvons garder les frequences par note
 * et aussi les différencier par octave.
 */
class Frequences
{
  private HashMap<String, Float> _freq;
  
  /**
   * Le Constructeur de la classe
   * Par default nous ajoutons l'octave <<normal>>
   */
  public Frequences() {
    _freq = new HashMap<String,Float>();
    
    _freq.put("Silence",0.0);
    
    _freq.put("C3", 262.0);
    _freq.put("C3#", 277.0);
    _freq.put("D3", 294.0);
    _freq.put("D3#", 311.0);
    _freq.put("E3", 330.0);
    _freq.put("F3", 349.0);
    _freq.put("F3#", 370.0);
    _freq.put("G3", 392.0);
    _freq.put("G3#", 415.0);
    _freq.put("A3", 440.0);
    _freq.put("A3#", 466.0);
    _freq.put("B3", 494.0);
  }

  /**
   * Nous ajoutons l'octave nécessaire pour notre morçeau
   * @param octave à ajouter
   */
  public void ajouterOctave(int octave)
  {
    if ( octave == 0)
    {
      _freq.put("C0", 32.7);
      _freq.put("C0#", 34.6);
      _freq.put("D0", 36.7);
      _freq.put("D0#", 38.9);
      _freq.put("E0", 41.2);
      _freq.put("F0", 43.6);
      _freq.put("F0#", 46.2);
      _freq.put("G0", 49.0);
      _freq.put("G0#", 51.9);
      _freq.put("A0", 55.0);
      _freq.put("A0#", 58.0);
      _freq.put("B0", 62.0);
    } else if ( octave == 1)
    {
      _freq.put("C1", 65.0);
      _freq.put("C1#", 69.0);
      _freq.put("D1", 74.0);
      _freq.put("D1", 78.0);
      _freq.put("E1", 83.0);
      _freq.put("F1", 87.0);
      _freq.put("F1#", 92.5);
      _freq.put("G1", 98.0);
      _freq.put("G1#", 104.0);
      _freq.put("A1", 110.0);
      _freq.put("A1#", 117.0);
      _freq.put("B1", 123.0);
    } else if ( octave == 2)
    {
      _freq.put("C2", 131.0);
      _freq.put("C2#", 139.0);
      _freq.put("D2", 147.0);
      _freq.put("D2#", 156.0);
      _freq.put("E2", 165.0);
      _freq.put("F2", 175.0);
      _freq.put("F2#", 185.0);
      _freq.put("G2", 196.0);
      _freq.put("G2#", 208.0);
      _freq.put("A2", 220.0);
      _freq.put("A2#", 233.0);
      _freq.put("B2", 247.0);
    } else if ( octave == 4)
    {
      _freq.put("C4", 523.0);
      _freq.put("C4#", 554.0);
      _freq.put("D4", 587.0);
      _freq.put("D4#", 622.0);
      _freq.put("E4", 659.0);
      _freq.put("F4", 698.0);
      _freq.put("F4#", 740.0);
      _freq.put("G4", 784.0);
      _freq.put("G4#", 831.0);
      _freq.put("A4", 880.0);
      _freq.put("A4#", 932.0);
      _freq.put("B4", 988.0);
    }
  }
  
  /**
   * On donne la frequence à partir de la note donné
   * On suppose qu'elle est sur la table de hash ou sinon on donne 440Hz 
   */
  public float getFreq(String note)
  {
    if (_freq.containsKey(note))
    {
      return _freq.get(note);
    }else
    {
      return _freq.get("A3");
    }
  }
}